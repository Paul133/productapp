#pragma once
#include <string>
#include <cstdint>
#include "IPriceble.h"

class Product: IPriceble
{
public:
	Product(uint16_t id, const std::string& name, float rawPrice);

	uint16_t getId() const;
	const std::string& getName() const;
	float getRawPrice() const;


private:
	uint16_t m_id;
	std::string m_name;
	float m_price;

};