#pragma once
#include "Product.h"
class PerishableProduct :
	public Product
{
public:

	PerishableProduct(uint16_t id, const std::string& name, float rawPrice, const std::string expirationDate);

	 int32_t getvat()const override;
	 float getPrice()const override;
	 std::string getExpirationdate()const;

private:
	std::string m_expirationDate;
};

