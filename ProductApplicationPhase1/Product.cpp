#include "Product.h"

Product::Product(uint16_t id, const std::string& name, float rawPrice) :
    m_id(id),
    m_name(name),
    m_price(rawPrice)
{
}

uint16_t Product::getId() const
{
    return m_id;
}

const std::string& Product::getName() const
{
    return m_name;
}

float Product::getRawPrice() const
{
    return m_price;
}
