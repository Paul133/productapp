#pragma once
#include <cstdint>

class IPriceble 
{
	virtual int32_t getvat()const=0;
	virtual float getPrice()const=0;
};